<?php



// function exceptionHandler($exception) {

//     // Clears any echo'd data which has already been shown
//     // More info: http://bit.ly/1FPGsU4
//     ob_end_clean();

//     // Show a human message to the user.
//     echo '<h1>Server error (500)</h1>';
//     echo '<p>Please contact your administrator, etc.</p>';

//     // Save a copy for the sys-admins ;)
//     error_log($exception->message);
// }

// set_exception_handler('exceptionHandler');


if (!function_exists('dx')){
    function dx(){ 
        vdx(func_get_args(), 0); }
}

if (!function_exists('vx')){
    function vx(){ 
        vdx(func_get_args(), 1); }
}

if (!function_exists('vdx')){
    function vdx($args, $ctu=0){
        echo "<pre>";
        foreach($args as $arg){
            var_export($arg);
        }
        echo "</pre>";
        if (!$ctu) die;
    }
}