<?php

namespace WPGuide;

use WPGuide\WpgException;

class Helpers {

    public static $VENUE_MAP = array('i'=>'NGVI', 'a'=>'NGVA','ngv'=>'NGV');
    public static $LEVEL_MAP = array('Ground Level'=>'grd', 'Level 1'=>'l1', 'Level 1 Mezzanine'=>'l1', 
        'Level 2'=>'l2', 'Level 3'=>'l3', 'Exhibition entrance'=>'grd');

    /** 
     * Transforms one array structure to another
     * 
     * @param   $data           array   A set of data.  Array of assoc arrays.
     * @param   $newStructure   array   Array map of how to draw the data from the
     *                                  $data array.  See below.
     * 
     * @return                  array   Array of transformed arrays
     * 
     * 
     * __More on $newStructure__
     * 
     * The array key will be used as the key for the new array, and the value
     * will calculate the new value based on the $data array.
     * 
     * String value: Simply grab the value in $data[$string]
     *               To reach into deeper arrays of $data, separate with "][" (e.g. below)
     * Function:     Closure function that is called with the full $data array
     *               as a single argument. Consider using array_mash within this function
     *               to return an array
     * 
     * $newStructure e.g.:
     * array_mash(
     *  'id'=>'post_id',
     *  'body'=>'post_content',
     *  'room-code'=>'fields][room_code',
     *  'complex'=>function($post){ return $post['simple'].$post['simple']; },
     *  'sections'=>array(
     *      // not implemented
     *  )
     * )
     * 
     */
    public static function array_mash($data, $newStructure){
            
        $mashed = array();
    
        foreach ($newStructure as $newKey=>$sourcePath) {
                if (is_array($sourcePath)){
                $mashed[$newKey] = Helpers::array_mash($data, $sourcePath);

            }else if ($sourcePath instanceof \Closure){
                $mashed[$newKey] = $sourcePath($data);
                
            }else if (is_object($sourcePath) || $sourcePath instanceof stdClass) {
                $mashed[$newKey] = Helpers::array_mash($data, (array)$sourcePath);

            }else{
                $sourcePathArr = explode("][", $sourcePath);
                $walkPosValue = $data;
                foreach ($sourcePathArr as $sourcePathStep) {
                    // if ($sourcePathStep == 'colors'){
                    //     echo 1;
                    // }
                    $walkPosValue = is_array($walkPosValue) 
                        ? $walkPosValue[$sourcePathStep] 
                        : ( is_object($walkPosValue) 
                                ? ( isset($walkPosValue->{$sourcePathStep}) && $walkPosValue->{$sourcePathStep} !== ""
                                    ? $walkPosValue->{$sourcePathStep} 
                                    : null
                                  )
                                : null
                        );
                }
                $mashed[$newKey] = $walkPosValue;
            }
        }

        return $mashed;

    }

    /**
     * Save postmeta field data of a wp_query result set, and convert posts to an array
     * 
     * @param   $queryPosts array   a WP_Query->posts array
     * @param   $fields     array   postmeta field names 
     * 
     * @return              array
     */
    public static function save_postmeta_fields($queryPosts, $fields){
        return array_map(
            function($post) use ($fields){
                return array_merge((array) $post, array_reduce(
                    $fields,
                    function ($carry, $item) use ($post) {
                        // vx($post->{$item});
                        return $carry + array_fill_keys(array($item), get_field($item, $post->ID));  
                    },
                    (array) $post
                ));
            },
            $queryPosts
        );
    }

    /**
     * With an array of (array) data, mix in some constant data
     * 
     * Really just array_maps array_merge
     * 
     * @param   $nodes  array   array of arbirtray associative arrays
     * @param   $fill   array   array of associative array
     * 
     * @return          array   resultant array
     */
    public static function fill_dummy_data($nodes, $fill){
        return array_map(function($exhi) use ($fill) {
            return $exhi + $fill;
        }, $nodes);
    }

    /**
     * Mask for file_get_contents. Will allow for extensibility for more complex
     * requirements.
     * 
     * @param   url     string      The url to hit
     * 
     * @return          stdClass    
     */
    public static function get_from_url($url){
        $rslt = file_get_contents($url);
        return (object) array(
            "status" => ($rslt !== false ? 1 : false),
            "data" => ($rslt !== false ? $rslt : null),
        );
    }

    public static function array_remove_empty($haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = Helpers::array_remove_empty($haystack[$key]);
            }
    
            // allows /false/ values
            if (is_null($haystack[$key]) || $haystack[$key] === "" || (is_array($haystack[$key]) && count($haystack[$key]) === 0)) {
                unset($haystack[$key]);
            }
        }
    
        return $haystack;
    }

    /**
     * Get the full descriptive string for location codes.
     * 
     * E.g. ngv_loc_map(array('venue'=>'i')); // "NGV International"
     *      ngv_loc_map(array('venue'=>'i', 'level'=>'l1')) // "Level 1"
     */
    public static function ngv_loc_map($filter){
        $locs = array(
            'venue'=>array(
                'i'=>'NGV International',
                'a'=>'NGV Australia'),
                'ngv'=>'NGV',
            'level'=>array(
                'grd'=>'Ground Level',
                'l1'=>'Level 1',
                'l2'=>'Level 2',
                'l3'=>'Level 3'
            )
        );
        // copy the 'levels' over both i and a
        $locs['level'] = array('i'=>$locs['level'], 'a'=>$locs['level']);

        if (array_key_exists('level', $filter)){
            return $locs['level'][strtolower($filter['venue'])][strtolower($filter['level'])];
        }else{
            return $locs['venue'][strtolower($filter['venue'])];
        }
    }

    /**
     * This function returns a function, which can be used as a callback array_mash
     * 
     * From a data object, $d, which will be passed in future, returns the value of 
     * the property, $v, if it is a string, or if it is an array, returns the value 
     * of the 0th element of that array.
     * 
     * Use case is because ESPA queries can return back data in either of the two formats:
     * 
     * array(
     *  'title'=>'Untitled picture of a boat',
     *  // OR sometimes (if the query specifies with fields to `return`)
     *  'title'=>array('Untitled picture of a boat')
     * )
     */
    public static function extractStrOrZth ($v){
        // $d never gets used.... it never, gets, used...
        return function($d) use ($v){
            return is_array($d->{$v}) ? $d->{$v}[0] : $d->{$v};
        };
    }

    /**
     * This function returns a function, which can be used as a callback for array_mash
     * 
     * From a data object, $d, which will be passed in future, looks for $v, then,
     * if $v is an array, looks at $v[0].  From the object at $d->$v or $d->v[0], 
     * returns the value of either ->content, or ->$k, if given. 
     */
    public static function extractZthContent ($v, $k='content'){
        return function($d) use ($v, $k){
            return property_exists($d, $v) ? 
                (is_array($d->{$v}) ? 
                    $d->{$v}[0]->{$k} : 
                    $d->{$v}->{$k}) 
                : null;
        };
    }

    public static function process_attached_image($attImg){
        $attImgArr = (array) $attImg;
        switch ($attImgArr['image_type']) {
            case 'Cumulus': 
                return array(
                    'type'=>'cumulus',
                    'imgId'=>$attImgArr['cumulus_image_id']
                );

            case 'Wordpress':
                return array(
                    'type'=>'wordpress',
                    'imgId'=>$attImgArr['wordpress_image']
                );

            case 'External':
                return array(
                    'type'=>'external',
                    'imgId'=>$attImgArr['custom_image_url']
                );
            
        }
    }

    public static function process_metafields($map){
        return function($data) use ($map){
            return array_reduce(array_keys($map), function ($carry, $newName) use ($map, $data){
                $x = array(
                    'name'  => $newName,
                    'value' => is_array($map[$newName]) ? $data->{$map[$newName]['src']} : $data->{$map[$newName]},
                    'type'  => is_array($map[$newName]) ? $map[$newName]['type'] : 'text',
                );
                return empty($x['value']) ? $carry : array_merge($carry, array($x));
            }, array());
        };
    }

}
