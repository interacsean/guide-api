<?php

namespace WPGuide;

class WpgException extends \Exception {

    // // Redefine the exception so message isn't optional
    public function __construct($message, $severity=0, $code = 0, Exception $previous = null) {
        // some code
    
        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }

    // // custom string representation of object
    public function __toString() {
        header('HTTP/1.1 400', true, 400);        
        return json_encode(array('status'=>'error',
            'message'=> $this->message));
    }

    // public function customFunction() {
    //     echo "A custom function for this type of exception\n";
    // }
}