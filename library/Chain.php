<?php

namespace Chain;

// use Chain\Chain as Chain;

/**
 * Chain provides a way to chain together functions or class methods, using the resulting value
 * of the previous call for the argument of the next 
 * 
 * TODO: check if no $CURVAL is passed, and use as first param.  Could use array_search or something
 */
class Chain {
    
    // Current value
    public $val;

    public static $CURVAL = null;
    
    /**
     * Initiate the chain with the starting value)
     */
    public function __construct($val=null){
        // CURVAL is set to a new, empty class so that only references to Chain::$CURVAL are matched
        // in the call's argument matching 
        if (self::$CURVAL === null){
            self::$CURVAL = new \StdClass();
        }
        $this->val = $val;
    }

    /**
     * Factory method to create new instance via static call
     * 
     * Useful for php 5.3 where you can't inline: (new Chain($x))->call(...)
     */
    public static function with($val){
        return new Chain($val);
    }
    
    /**
    * Call the function $fn with the current value as an argument.  
    * 
    * To call a class or object method ([$object, 'methodName']), pass an array of two elements for $fn.
    * If other arguments are required, you can create a pseudo closure by specifying up to two additional
    * arguments: $argsBefore and $argsAfter.  For example if a function or method takes the following 
    * parameters: multiAddSubDivide($m, $a, $s, $d){ return ($m*$a+$s)/$d; }
    *  ...and you want to call multiAddSubDivide(5, {current-value}, 8, 10); 
    * you would call: ->to('multiAddSubDivide', array(5), array(8, 10);
    */
    public function call(){ // , $argsBefore=array(), $argsAfter=array()//
        $cur_val = $this->val;
        $this->val = call_user_func_array(
            func_get_arg(0), 
            count(func_get_args()) === 1
                ? array($cur_val) 
                : array_map(function($arg) use ($cur_val) {
                    return ($arg === Chain::$CURVAL) ? $cur_val : $arg;
                }, array_slice(func_get_args(), 1))
        );
        //array_merge($argsBefore===null ? array() : $argsBefore,array($this->val),$argsAfter));
        return $this;
    }

    /**
     * Optional alias of call
     */
    public function thenCall(){
        return call_user_func_array(array($this, 'call', func_get_args()));
    }
    
    /** 
     * TODO
     * 
     * Provides a means to reach into a property of an object, or element of an array
     * 
     * E.g. $path = "->some_prop[0]['named_key']"
     */
    public function getAt($path){

    }

    /**
     * For debugging, you can call dumpVal() during the chain to output the current value
     */
    public function dumpVal($nl = false){
        var_export($this->val);
        if ($nl) echo "\n";
        return $this;
    }

    /**
     * If value is String, or toString-able, can be used like
     * echo (new Chain('d'))->call(function($str){ return $str.$str; })
     * 
     * i.e. without finishing with ->val
     */
    public function __toString(){
        return $this->val;
    }

}
