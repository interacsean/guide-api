<?

use WPGuide\Helpers;
use \WorksController;

class SpaceController {
    
    /**
     * Get single space by ID
     * 
     * @params  $params  array   AltoRouter path params.  Should contain 'name'
     * @returns          string  JSON string representing the Space
     */
    public function get_space($params) {

        $args = array(
            'post_type' => 'galleryspace',
            'posts_per_page' => 1,
            'meta_query' => SpaceController::space_query_code($params['name']),
        );
        header('Content-Type: application/json');
        echo json_encode( (object) array_shift($this->process_spaces($args, array('full'=>true))) );
    }
    
    /**
     * Get multiple spaces by space location path
     * 
     * @params  $params  array   AltoRouter path params for varying specificity
     *     Can optionally contain: venue, level, space, subcode, return
     * @returns          string  JSON string representing the Spaces
     */
    public function get_spaces($params) {
        // If no params are specified, return all Spaces
        if (empty($params['ngv'])){
            
            $args = array(
                'post_type' => 'galleryspace',
                'posts_per_page' => -1,
            );

        // first parameter is not 'ngv', assume it's a full, hyphenated identifier and use get_space
        }else if (strtolower($params['ngv']) !== 'ngv'){
            return $this->get_space(array('name'=>$params['ngv']));

        // Partial location given
        }else{
            $args = array(
                'post_type' => 'galleryspace',
                'posts_per_page' => -1,                    
                'meta_query'=> SpaceController::space_query_code(
                    array('ngv', $params['venue'], $params['level'], $params['space'], isset($params['subcode']) ? $params['subcode'] : null)
                )
            );
        }

        if ($params['return'])
            return $this->process_spaces($args, $params);
        else{
            header('Content-Type: application/json');
            echo json_encode( (object) array('spaces'=> $this->process_spaces($args)) );
        }
    }

    /**
     * With a set of wp_query args, return a (set of) formatted Spaces for response
     * 
     * @param  $args  array  Wordpress WP_Query args array
     * @param  $cfg   array  Optional configuration flags
     * @returns       array  Transformed Spaces
     */
    private function process_spaces( $args, $cfg=array() ) {
        $cfg += array('full'=>false, 'no_works'=>false, 'no_adjacent'=>false);
        
        $spcQuery = new WP_Query( $args );
        $spaces = $spcQuery->posts;

        $ct=0;
        // array_map for each of the $spaces returned
        $transformedSpaces = array_map(
            function ($space) use ($cfg, &$ct) {
                if (!empty($space->artwork_list) && !$cfg['no_works']){
                    $workIDs = SpaceController::get_works_of_worklists($space->artwork_list);

                    try {
                        $artworkData = Helpers::get_from_url("https://www.ngv.vic.gov.au/custom/espa/collection/works?".
                        "query=(image_available:true)".
                            (!$cfg['full'] ? "%20AND%20(_exists_:featured)" : '').
                            "%20AND%20(".implode("%20OR%20", $workIDs).")&filters=_id&size=200".
                            WorksController::get_required_espa_fields_param());
                        
                        if (!$artworkData->data) return array();
                        
                        $workList = WorksController::parse_espa_works($artworkData->data, 'basic');

                    }catch(\Error $e){
                        // TODO: do something with $e;
                        $workList = array();
                    }
                }else $workList = array();

                // Transform the result data into the desired form.
                $mashedSpace = Helpers::array_mash($space, array( // mash map
                    'venueCode' 				=>	function($d){ 
                        return strtolower($d->venue); 
                    },
                    'venueName'                 =>  function($d) { 
                        return Helpers::ngv_loc_map(array('venue'=>$d->venue));
                    },
                    'levelCode'					=>	function($d){ 
                        return strtolower($d->level); 
                    },
                    'levelName'					=>	function($d){ 
                        return Helpers::ngv_loc_map(array('venue'=>$d->venue, 'level'=>$d->level)); 
                    },
                    'spaceCode'					=>	function($d){ 
                        return strtolower($d->room_code); 
                    },
                    'spaceName'				    =>	'public_name',
                    'mapName'                   =>  'map_name',
                    'subRoomCode'				=>	function($d){ 
                        return strtolower($d->{"sub-room_code"}); 
                    },
                    'spaceCodeSlashed'			=>	function($d){
                        return strtoupper("NGV/{$d->venue}/{$d->level}/{$d->room_code}"
                            . (!empty($d->{"sub-room_code"}) ? "/{$d->{"sub-room_code"}}" : ''));
                    },
                    'spaceCodeFull'	=>	function($d){
                        return SpaceController::construct_space_id($d);
                    },
                    'adjacentSpaces'            =>  function($d) use ($cfg){ 
                        if ($cfg['no_adjacent']) return null;

                        $adjSpcs = get_field('adjacent_spaces', $d->ID);
                        return $adjSpcs && count($adjSpcs) > 0 ? array_map(function($postId){ 
                            $p = get_post($postId);
                            return SpaceController::construct_space_id($p);
                            
                        }, $adjSpcs[0]['adjacent_space']) : null; 
                    },
                    'tagline'					=>	'tagline',
                    'roomStatus'				=>	'room_status',
                    'relatedExhibition'		    =>	function($d){ 
                        return $d->related_exhibition[0]; 
                    },
                    'imageHero' 				=>	'hero_image',
                    'featuredArtworks'          =>  function($d) use ($cfg, $workList){ 
                        if ($cfg['full']){
                            return array_values(array_filter($workList, function($wk){
                                return $wk['featured'];
                            }));

                        // if !cfg['full'], workList will only include featured anyway
                        } else return $workList; 
                    },
                    'artworks'                  =>  function($d) use ($cfg, $workList) { 
                        if ($cfg['full']){
                            return array_values(array_filter($workList, function($wk){
                                return !$wk['featured'];
                            }));
                        } else return null;
                    },
                    'id'=>'ID',
                    'description'=>'post_content',
                    'descriptionMore'=>'more',
                ));
                
                return $mashedSpace;
            },
            $spaces
        );

        $trimmedSpaces = array_map(function($t){ return Helpers::array_remove_empty($t) ;}, $transformedSpaces);

        $spacesWTheme = SpaceController::add_space_theme_from_exhibitions($trimmedSpaces);

        return array_values(Helpers::array_remove_empty($spacesWTheme));
    }

    /**
     * From an array of worklists (wordpress field reference), get a flat list of the artwork IDs
     * 
     * @param  $lists  array  Array of wordpress worklists
     * @returns        array  Flat array of Artwork IDs
     */
    public static function get_works_of_worklists($lists) {
        if (!is_array($lists)) return false;

        return array_reduce($lists, function($carry, $listId) {
            $worklist = get_post($listId);
            $workIds = get_field('works', $listId);
            return array_merge(
                $carry, 
                array_reduce(
                    $workIds,
                    function($wksCarry, $wk){
                        array_push($wksCarry, trim($wk['work_id']));
                        return $wksCarry;
                    },
                    array()
                )
            );
        }, array() );

    }

    /**
     * Add the related exhibition theme for an array of spaces
     * 
     * @param  $spaces  array  Array of spaces
     * @returns         array  The array of spaces with exhibition theme data added
     */
    public static function add_space_theme_from_exhibitions($spaces){

        // Get array of exhibition IDs for the given spaces
        $exhiIds = array_reduce($spaces, function($exhiIds, $space){
            return $space['relatedExhibition'] ? 
                $exhiIds + array((int)$space['relatedExhibition']) : 
                $exhiIds;
        }, array());

        // Fetch the wordpress posts of those IDs
        $exhis = get_posts(array(
            'post_type'=>'exhibition',
            'post__in' => $exhiIds,
        ));

        // Map the results in assoc-array by Exhib ID
        $exhisById = array_reduce($exhis, function($exhisById, $exhiObj){ 
            return $exhisById+array($exhiObj->ID=>$exhiObj); 
        }, array());

        // For each space, look up related exhibition and merge in the theme.
        return array_map(function($space) use ($exhisById){
            if ($space['relatedExhibition'] 
                && ($exhisById[$space['relatedExhibition']]->takeover_bg_colour
                    || $exhisById[$space['relatedExhibition']]->takeover_fg_colour 
                )){
                $newTheme = array();
                if (!empty($exhisById[$space['relatedExhibition']]->takeover_bg_colour)) {
                    $newTheme['bgColor'] = $exhisById[$space['relatedExhibition']]->takeover_bg_colour;
                }
                if (!empty($exhisById[$space['relatedExhibition']]->takeover_fg_colour)) {
                    $newTheme['fgColor'] = $exhisById[$space['relatedExhibition']]->takeover_fg_colour;
                }
                return $space + array('theme'=>$newTheme);
            }else {
                return $space;
            }

        }, $spaces);

    }
    
    /**
     * Form the wp_query arg for meta_query
     * 
     * @param  $spaceIdm  mixed   Either string (e.g. 'ngv-i-l2-e16c')
     *                            or array (e.g. array('ngv','i','l2'))
     * @param  $dlmt      string  If string is passed for $spaceIdm, use this to split
     * @returns           array   WP_Query meta_query arg
     */
    public static function space_query_code($spaceIdm, $dlmt = '-') {
        $spaceParts = array_slice(is_array($spaceIdm) ? $spaceIdm : explode($dlmt, $spaceIdm), 1); // remove the 'ngv' part
        $partFields = array('venue','level','room_code','sub-room_code');

        return array_reduce(array_keys($spaceParts), function($carry, $partKey) use ($spaceParts, $partFields) {
            return empty($spaceParts[$partKey]) ? $carry :
                array_merge($carry, array(array('key'=>$partFields[$partKey], 'value'=>strtoupper($spaceParts[$partKey]))));
        }, array() );
    }

    /**
     * Construct a Space identifier from data object
     * 
     * @param  $d  object  Containing props: venue, level, room_code, [sub-room_code]
     */
    public static function construct_space_id($d){
        return strtolower("NGV-{$d->venue}-{$d->level}-{$d->room_code}"
        . (!empty($d->{"sub-room_code"}) ? "-{$d->{"sub-room_code"}}" : ''));
    }
    
}

?>