<?php

use PHPUnit\Framework\TestCase;
use WPGuide\Helpers;

// require_once('library/Helpers.php');

global $BASE_PATH;
$BASE_PATH = "http://127.0.0.1:8888/custom/guidewp17";
// $BASE_PATH = "https://www.ngv.vic.gov.au/custom/guidewp17";

class SpacesTest extends TestCase {

    public function _testSpacesWith($endpoint, $qsParams, $assertions){

        global $BASE_PATH;

        $rsltRaw = file_get_contents($BASE_PATH.$endpoint.http_build_query($qsParams));

        $rsltArr = json_decode($rsltRaw, true);
        // dd($rsltRaw);
        $this->assertNotEquals($rsltArr, null);
        
        $assertions($rsltArr);
        
    }

    public function testIndiSpace() {

        $self = $this;
        $this->_testSpacesWith('/space/ngv-i-l2-e16b?ngvts='.time(), array(), function($rslt) use ($self){ 
        
            $keys = array('venueCode','venueName','levelCode','levelName',
                'spaceCode','spaceCodeSlashed','spaceCodeFull',
                'roomStatus','id', 'artworks','featuredArtworks'
            );
            global $STRICT_DATA;
            if ($STRICT_DATA){
                $keys += array('mapName','spaceName');
                //'adjacentSpaces','description',
            }
            foreach($keys as $key){
                try {
                    $self->assertArrayHasKey($key, $rslt);
                } catch (PHPUnit_Framework_AssertionFailedError $e) {
                    throw new \Exception("ngv-i-l2-e16b did not have $key\n".var_export($rslt, 1));
                } 
            }
        });
    }

    public function testAllRequest() {

        $self = $this;
        $this->_testSpacesWith('/spaces/?ngvts='.time(), array(), function($rsltset) use ($self){
            $self->assertArrayHasKey('spaces', $rsltset);          
            $isArray = is_array($rsltset['spaces']);
            $self->assertTrue($isArray);      
        
            $self->assertGreaterThan(0, count($rsltset['spaces']));   
            
            foreach ($rsltset['spaces'] as $k=>$rslt) {
                $keys = array('venueCode','venueName','levelCode','levelName',
                    'spaceCode','spaceCodeSlashed','spaceCodeFull',
                    'roomStatus','id'
                );
                global $STRICT_DATA;
                if ($STRICT_DATA){
                    $keys += array('mapName','spaceName');
                    //'adjacentSpaces','description',
                }
                foreach($keys as $key){
                    try {
                        $self->assertArrayHasKey($key, $rslt);
                    } catch (PHPUnit_Framework_AssertionFailedError $e) {
                        throw new \Exception("$k did not have $key\n".var_export($rslt, 1));
                    } 
                }
            }
        });

    }

}