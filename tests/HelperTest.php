<?php

use PHPUnit\Framework\TestCase;
use WPGuide\Helpers;

class HelpersTest extends TestCase {

    public function testHelper(){

        $this->assertEquals(
            Helpers::array_mash(
                // data
                array(
                    'post_id'=>'44',
                    'post_content'=>'Yo yo yo',
                    'fields'=>array(
                        'room_code'=>'tx1'
                    )
                ),
                // mash_map
                array(
                    'id'=>'post_id',
                    'body'=>'post_content',
                    'room-code'=>'fields][room_code',
                )
            ), 
            // expected
            array(
                'id'=>'44',
                'body'=>'Yo yo yo',
                'room-code'=>'tx1',
            )
        );
    }

}
