<?php

use PHPUnit\Framework\TestCase;
use WPGuide\Helpers;

// require_once('library/Helpers.php');

global $BASE_PATH;
// $BASE_PATH = "http://127.0.0.1:8888/custom/guidewp17";
$BASE_PATH = "https://www.ngv.vic.gov.au/custom/guidewp17";

/** /!\ /!\ /!\  The wp-config throws an error
 * 
 * As such, I'm using file_get_contents instead to actually hit the API through an HTTP request

require_once('./wp-config.php');
require 'controllers/ExhibitionsController.php';

// Store original version of GET
global $o_GET;
$o_GET = $_GET;

 **/

class ExhibitionsTest extends TestCase {

    // /**
    //  * @before
    //  */
    // public function resetGet(){
    //     global $o_GET;
    //     $_GET = $o_GET;
    // }
    
    public function _testExhibsWith($endpoint, $qsParams, $assertions, $cfg=array()){
        $cfg += array('minResults'=>0);

        global $BASE_PATH;

        $rsltRaw = file_get_contents($BASE_PATH.$endpoint.http_build_query($qsParams));

        // $testValue_venue = 'i';
        // // set up optional qs params
        // $_GET['venue'] = $testValue_venue;

        // $rsltRaw = ExhibitionsController::get_exhibitions($pathParams);

        $rsltArr = json_decode($rsltRaw, true);
        $this->assertNotEquals($rsltArr, null);
        $this->assertArrayHasKey('exhibitions', $rsltArr);                
        
        if ($rsltArr['exhibitions'] !== null) {
            $isArray = is_array($rsltArr['exhibitions']);
            $this->assertTrue($isArray);

            if ($isArray){
                $this->assertGreaterThan($cfg['minResults'], count($rsltArr['exhibitions']));     
                foreach ($rsltArr['exhibitions'] as $rslt) {
                    $assertions($rslt);
                }
            }
        }
        
    }


    public function testNGVIrequest() {

        $self = $this;
        $this->_testExhibsWith('/exhibitions/?', array('venue'=>'i'), function($rslt) use ($self){
            $keys = array('id','venueName','title','dateStart','dateEnd','venueCode');
            global $STRICT_DATA;
            if ($STRICT_DATA){
                $keys += array('levelCode','levelName','description','infoSummary','subtitle');
            }
            // ,'venueName','levelCode','levelName','description','infoSummary','subtitle');
            foreach($keys as $key){
                $self->assertArrayHasKey($key, $rslt); 
            }
            $self->assertEquals('i', $rslt['venueCode']);
        }, array('minResults'=>1));

    }

    public function testCurrentRequest() {

        $self = $this;
        $this->_testExhibsWith('/exhibitions/?', array('now-showing'=>'current,upcoming'), function($rslt) use ($self){
            $self->assertArrayHasKey('dateStart', $rslt);          
            $self->assertGreaterThan(date('Ymd'), $rslt['dateEnd']);
        });

    }

}