<? 
	
	require 'vendor/autoload.php';
	require_once($_SERVER['DOCUMENT_ROOT'].'/wp-config.php');

	require 'library/bootstrap.php';

	require 'controllers/SetController.php';
	require 'controllers/VisitController.php';
	require 'controllers/ImageController.php';
	require 'controllers/SocialController.php';
	require 'controllers/ExhibitionController.php';
	require 'controllers/WorksController.php';
	require 'controllers/SpaceController.php';
	require 'controllers/PlaylistController.php';
	require 'controllers/EventController.php';
	require 'controllers/MapController.php';

	$router = new AltoRouter();
	$router->setBasePath('/custom/guidewp17');
	
	$router->map( 'GET', '/set/?', 						'SetController#get_latest' );
	$router->map( 'GET', '/sets/?',     				'SetController#get_sets' );
	$router->map( 'GET', '/sets/[:site]/?',     		'SetController#get_sets' );
	$router->map( 'GET', '/sets/[:site]/[:type]/?',     'SetController#get_sets' );
	$router->map( 'GET', '/set/[:slug]/?', 				'SetController#get_set' );
	$router->map( 'GET', '/room/[:name]/?', 			'SetController#get_room' );
	$router->map( 'GET', '/image/[i:id]/?', 			'ImageController#get_image' );
	$router->map( 'GET', '/sites/?', 					'VisitController#get_sites' );
	$router->map( 'GET', '/social/latest-tweets/[i:num]/?','SocialController#get_latestTweets' );

	$router->map( 'GET', '/space/[:name]/?', 			'SpaceController#get_space' );
	$router->map( 'GET', '/spaces/[:ngv]?/?[:venue]?/?[:level]?/?[:space]?/?[:subcode]?/?',	'SpaceController#get_spaces' );
	$router->map( 'GET', '/exhibitions/?',				'ExhibitionController#get_exhibitions' );
	$router->map( 'GET', '/exhibition/[:exhibitionId]/?','ExhibitionController#get_exhibition' );
	$router->map( 'GET', '/work/[:workId]/?',			'WorksController#get_work' );
	$router->map( 'GET', '/works/?',					'WorksController#get_works' );
	$router->map( 'GET', '/playlist/[:playlistId]/?',	'PlaylistController#get_playlist' );
	$router->map( 'GET', '/playlists/?',				'PlaylistController#get_playlists' );
	$router->map( 'GET', '/event/[:eventId]/?',			'EventController#get_event' );
	$router->map( 'GET', '/events/?',					'EventController#get_events' );
	$router->map( 'GET', '/map/?',						'MapController#get_mapData' );
	
	
	$match = $router->match();
	
	// get our controller and method in $controller and $method vars
	list( $controller, $method ) = explode( '#', $match['target'] );
	
	// if controller->action is callable then make the call and pass params
	if ( is_callable(array($controller, $method)) ) {
	    $obj = new $controller();
	    call_user_func_array(array($obj,$method), array($match['params']));
	} else {
		header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
		//echo 'Error: can not call '.$controller.'->'.$method;
	}
	
?>